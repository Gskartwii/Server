package main

import (
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

// routeGetCharacter: impl for GET /api/characters/:characterid
func routeGetCharacter(c *gin.Context) {
	characterid, err := strconv.Atoi(c.Param("characterid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad character id",
		})
		return
	}
	var result character
	err = getCharacter.Get(&result, characterid)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Database error",
		})
	} else if result.ID == 0 {
		c.JSON(404, gin.H{
			"error": "No such character id",
		})
	} else {
		c.JSON(200, result)
	}
}

// routePatchCharacter: impl for PATCH /api/characters/:characterid
func routePatchCharacter(c *gin.Context) {
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}
}

// routeDeleteCharacter: impl for DELETE /api/characters/:characterid
func routeDeleteCharacter(c *gin.Context) {
	// Actually delete the character
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}
	if !strings.HasPrefix(auth, "Service") {
		c.JSON(403, gin.H{
			"error": "Only services may delete characters",
		})
	}
	charid, err := strconv.Atoi(c.Param("characterid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad character id format",
		})
	}
	trusted, err := redisConnection.Do("GET", "trusted-characters:"+auth)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Internal database error",
		})
	}
	if trusted.(bool) {
		deleteCharacter.Exec(charid)
		c.JSON(200, gin.H{})
	} else {
		c.JSON(403, gin.H{
			"error": "Your account is not trusted to delete characters",
		})
	}
}

// routeGetCharacterGallery: impl for GET /api/characters/:characterid/gallery
func routeGetCharacterGallery(c *gin.Context) {
	characterid, err := strconv.Atoi(c.Param("characterid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad character id",
		})
		return
	}
	var result []media
	err = getCharacterMedia.Select(&result, characterid)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Database error",
		})
	} else {
		c.JSON(200, result)
	}
}

// routePostCharacterGallery: impl for POST /api/characters/:characterid/gallery?id=<int64>
func routePostCharacterGallery(c *gin.Context) {
	// ?id=int64
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}
	// Create a new link between media and the character
}

// routeDeleteFromCharacterGallery: impl for DELETE /api/characters/:characterid/gallery?id=<int64>
func routeDeleteFromCharacterGallery(c *gin.Context) {
	// ?id=int64
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}
}

// routeGetCharacterGalleryByOrigin: impl for GET /api/characters/:characterid/gallery/:origin
func routeGetCharacterGalleryByOrigin(c *gin.Context) {
	characterid, err := strconv.Atoi(c.Param("characterid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad character id",
		})
		return
	}
	originid, err := strconv.Atoi(c.Param("characterid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad origin id format",
		})
		return
	}
	var result []media
	err = getCharacterMediaByOrigin.Select(&result, characterid, originid)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Database error",
		})
	} else {
		c.JSON(200, result)
	}
}

// routePostCharacter: impl for POST /api/character
func routePostCharacter(c *gin.Context) {
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}
	istrusted, err := redisConnection.Do("EXISTS", "trusted-characters:"+auth)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Internal database error",
		})
		return
	}
	if istrusted.(bool) == false {
		c.JSON(403, gin.H{
			"error": "Account is not authorized to create unowned characters",
		})
		return
	}
	name := c.PostForm("name")
	species := c.PostForm("species")
	if name == "" || species == "" {
		c.JSON(400, gin.H{
			"error": "Name and species can not be empty",
		})
		return
	}
	characterSnowflake := snowgen.Generate().Int64()
	_, err = createCharacter.Exec(characterSnowflake, name, species)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Internal database error",
		})
	} else {
		c.JSON(200, gin.H{
			"id": characterSnowflake,
		})
	}
}
