package main

// import "github.com/globalsign/mgo/bson"

/*
	UserAccount (
		id BIGINT UNSIGNED NOT NULL,
		username CHAR(32) NOT NULL,
		pass_hash BINARY(64) NOT NULL,
		CONSTRAINT PRIMARY KEY (id),
		CONSTRAINT UNIQUE (username),
		INDEX (username)
	)
*/

type userAccount struct {
	// Ask JSON to ignore so that it can't be /accidentally/ sent
	ID       uint64 `db:"id" json:"-"`
	Username string `db:"username" json:"-"`
	PassHash []byte `db:"pass_hash" json:"-"`
}

/*
	UserData (
		id BIGINT UNSIGNED NOT NULL,
		username CHAR(32) NOT NULL,
		description VARCHAR(255),
		name VARCHAR(32),
		fursona BIGINT UNSIGNED,
		avatar_url VARCHAR(255),
		CONSTRAINT PRIMARY KEY (id),
		CONSTRAINT FOREIGN KEY (id)
			REFERENCES UserAccount (id)
			ON DELETE CASCADE,
		CONSTRAINT FOREIGN KEY (username)
			REFERENCES UserAccount (username)
			ON UPDATE CASCADE
	)
*/

type userData struct {
	ID          uint64 `db:"id" json:"id"`
	Username    string `db:"username" json:"username"`
	Description string `db:"description" json:"description"`
	Name        string `db:"name" json:"name"`
	FursonaID   uint64 `db:"fursona_id" json:"fursona_id"` // Not a nested Character
	AvatarURL   string `db:"avatar_url" json:"avatar_url"`
}

/*
	Identity (
		origin BIGINT UNSIGNED NOT NULL,
		user_id BIGINT UNSIGNED NOT NULL,
		external_id VARCHAR(64) NOT NULL,
		CONSTRAINT PRIMARY KEY (user_id, origin),
		INDEX (external_id, origin)
	)
*/

type identity struct {
	Origin     uint64 `db:"origin" json:"origin"`
	UserID     uint64 `db:"user_id" json:"user_id"`
	ExternalID string `db:"external_id" json:"external_id"`
}

/*
	Media (
		id BIGINT UNSIGNED NOT NULL,
		icon_url VARCHAR(255) NOT NULL,
		sample_url VARCHAR(255) NOT NULL,
		source_url VARCHAR(255) NOT NULL,
		name VARCHAR(64) NOT NULL,
		type ENUM("image", "audio", "video", "text") NOT NULL,
		origin BIGINT UNSIGNED NOT NULL,
		CONSTRAINT PRIMARY KEY (id)
	)
*/

type media struct {
	ID        uint64 `db:"id" json:"id"`
	IconURL   string `db:"icon_url" json:"icon_url"`
	SampleURL string `db:"sample_url" json:"sample_url"`
	SourceURL string `db:"source_url" json:"source_url"`
	Name      string `db:"name" json:"name"`
	Type      string `db:"type" json:"type"`
	Origin    uint64 `db:"origin" json:"origin"`
}

/*
	Character (
		id BIGINT UNSIGNED NOT NULL,
		name VARCHAR(64) NOT NULL,
		description VARCHAR(255),
		species VARCHAR(32) NOT NULL,
		owner_id BIGINT UNSIGNED,
		preview_image BIGINT UNSIGNED,
		CONSTRAINT PRIMARY KEY (id),
		CONSTRAINT FOREIGN KEY (owner_id)
			REFERENCES UserAccount (id)
			ON DELETE SET NULL,
		CONSTRAINT FOREIGN KEY (preview_image)
			REFERENCES Media (id)
			ON DELETE SET NULL
	)
*/

type character struct {
	ID          uint64 `db:"id" json:"id"`
	Name        string `db:"name" json:"name"`
	Description string `db:"description" json:"description"`
	Species     string `db:"species" json:"species"`
	OwnerID     uint64 `db:"owner_id,omitempty" json:"owner_id,omitempty"`
}

/*
	UserMedia (
		user_id BIGINT UNSIGNED NOT NULL,
		media_id BIGINT UNSIGNED NOT NULL,
		CONSTRAINT PRIMARY KEY (user_id, media_id),
		CONSTRAINT FOREIGN KEY (user_id)
			REFERENCES UserAccount (id)
			ON DELETE CASCADE,
		CONSTRAINT FOREIGN KEY (media_id)
			REFERENCES Media (id)
			ON DELETE CASCADE
	)
*/

/*
	CharacterMedia (
		character_id BIGINT UNSIGNED NOT NULL,
		media_id BIGINT UNSIGNED NOT NULL,
		CONSTRAINT PRIMARY KEY (character_id, media_id),
		CONSTRAINT FOREIGN KEY (character_id)
			REFERENCES Character (id)
			ON DELETE CASCADE,
		CONSTRAINT FOREIGN KEY (media_id)
			REFERENCES Media (id)
			ON DELETE CASCADE
	)
*/
