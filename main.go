package main

import (
	"encoding/base64"
	"os"
	"strconv"

	"github.com/bwmarrin/snowflake"
	"github.com/gin-gonic/gin"
	"github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	//"github.com/globalsign/mgo"
	//"github.com/globalsign/mgo/bson"

	"github.com/gomodule/redigo/redis"
	uuid "github.com/satori/go.uuid"
)

var (
	findLogin,
	existsLogin,

	createLogin,
	createUserData,
	// createMedia,
	createCharacter,
	createCharacterWithOwner,

	getUserData,
	getCharacter,
	getUserCharacters,
	getFursona,
	getMedia,
	getUserMedia,
	getUserMediaByOrigin,
	getCharacterMedia,
	getCharacterMediaByOrigin,

	deleteMedia,
	deleteCharacter,
	disownCharacter *sqlx.Stmt
)
var sqlConnection *sqlx.DB
var snowgen *snowflake.Node
var redisConnection redis.Conn // interface type

func init() {
	noden, _ := strconv.Atoi(os.Getenv("SNOWFLAKE_NODE_ID"))
	snowgen, _ = snowflake.NewNode(int64(noden))
	redisConnection, _ = redis.DialURL(os.Getenv("REDIS_URL"))

	mariaconfig := mysql.NewConfig()
	mariaconfig.User = os.Getenv("MARIA_USER")
	mariaconfig.Passwd = os.Getenv("MARIA_PASS")
	mariaconfig.Addr = os.Getenv("MARIA_ADDRESS")
	sqlConnection = sqlx.MustConnect("mysql", mariaconfig.FormatDSN())

	findLogin, _ = sqlConnection.Preparex("SELECT * FROM UserAccounts WHERE username=?")
	existsLogin, _ = sqlConnection.Preparex("SELECT count(1) FROM UserAccounts WHERE username=?")

	createLogin, _ = sqlConnection.Preparex("INSERT INTO UserAccounts (id, username, pass_hash) VALUES (?, ?, ?)")
	createUserData, _ = sqlConnection.Preparex("INSERT INTO UserData (id, username) VALUES (?, ?)")
	// createMedia, _     = sqlConnection.Preparex("INSERT INTO Media VALUES (?, ?, ?, ?, ?, ?, ?)")
	createCharacter, _ = sqlConnection.Preparex("INSERT INTO Character (id, name, species) VALUES (?, ?, ?)")
	createCharacterWithOwner, _ = sqlConnection.Preparex("INSERT INTO Character (id, name, species, owner_id) VALUES (?, ?, ?, ?)")

	getUserData, _ = sqlConnection.Preparex("SELECT * FROM UserData WHERE id=?")
	getCharacter, _ = sqlConnection.Preparex("SELECT * FROM Characters WHERE id=?")
	getUserCharacters, _ = sqlConnection.Preparex("SELECT * FROM Characters WHERE owner_id=?")
	getFursona, _ = sqlConnection.Preparex("SELECT Characters.* FROM Characters JOIN UserData ON UserData.fursona_id = Characters.id WHERE UserData.id=?")
	getMedia, _ = sqlConnection.Preparex("SELECT * FROM Media WHERE id=?")
	getUserMedia, _ = sqlConnection.Preparex("SELECT Media.* FROM Media JOIN UserMedia ON UserMedia.media_id = Media.id WHERE UserMedia.user_id=?")
	getUserMediaByOrigin, _ = sqlConnection.Preparex("SELECT Media.* FROM Media JOIN UserMedia ON UserMedia.media_id = Media.id WHERE UserMedia.user_id=? AND Media.origin=?")
	getCharacterMedia, _ = sqlConnection.Preparex("SELECT Media.* FROM Media JOIN CharacterMedia ON CharacterMedia.media_id = Media.id WHERE CharacterMedia.character_id=?")
	getCharacterMediaByOrigin, _ = sqlConnection.Preparex("SELECT Media.* FROM Media JOIN CharacterMedia ON CharacterMedia.media_id = Media.id WHERE CharacterMedia.character_id=? AND Media.origin=?")

	deleteMedia, _ = sqlConnection.Preparex("DELETE FROM Media WHERE id=? AND origin=?")
	deleteCharacter, _ = sqlConnection.Preparex("DELETE FROM Characters WHERE id=?")
	disownCharacter, _ = sqlConnection.Preparex("UPDATE Characters SET owner_id=NULL WHERE id=? AND owner_id=?")
}

func generateBearerSessionDB(redb redis.Conn) string {
	var sessionidstr string
	for {
		sessionid := uuid.Must(uuid.NewV4())
		sessionidstr = "Bearer " + base64.StdEncoding.EncodeToString(sessionid.Bytes())
		exists, _ := redb.Do("EXISTS", "sessions:"+sessionidstr)
		if !exists.(bool) {
			break
		}
	}
	return sessionidstr
}

func main() {
	router := gin.Default()
	router.StaticFile("/login", "static/html/login.html")
	router.POST("/login", routeLogin)

	router.StaticFile("register", "static/html/register.html")
	router.POST("/register", routeRegister)

	user := router.Group("/user")
	{ // User
		user.StaticFile("/@me", "static/html/myuserpage.html")
		user.GET("/:userid", func(c *gin.Context) {
			c.File("static/html/userpage.html")
		})
		user.StaticFile("/settings", "static/html/settings.html")
		user.PATCH("/settings", func(c *gin.Context) {
			// Because this will only ever be accessed by users, it goes in the user
			// section. Otherwise, most data posting belongs in the API.
		})
		user.GET("/:userid/gallery", func(c *gin.Context) {
			c.File("static/html/gallery.html")
		})
		user.GET("/:userid/characters", func(c *gin.Context) {
			c.File("satic/html/characters.html")
		})
	}

	api := router.Group("/api")
	{ // API
		userAPI := api.Group("/user/:userid")
		mediaAPI := api.Group("/media/:mediaid")
		characterAPI := api.Group("/character/:characterid")
		api.GET("/user/@me", routeGetSelfUser)
		{ // User API
			userAPI.GET("/", routeGetUser)
			userAPI.PATCH("/", routePatchUser)

			userAPI.GET("/gallery", routeGetUserGallery)
			userAPI.GET("/gallery/:origin", routeGetUserGalleryByOrigin)
			userAPI.POST("/gallery", routePostUserGallery)

			userAPI.GET("/fursona", routeGetFursona)
			userAPI.GET("/characters", routeGetUserCharacters)
			userAPI.POST("/characters", routePostUserCharacters)
			userAPI.PATCH("/characters/:charid", routePatchUserCharacter)
			userAPI.DELETE("/characters/:charid", routeDeleteUserCharacter)
		}
		{ // Media API
			mediaAPI.GET("/", routeGetMedia)
			mediaAPI.DELETE("/", routeDeleteMedia)
			// Media is immutable after it hits the database, because it is *unowned*
		}
		{ // Character API
			characterAPI.GET("/", routeGetCharacter)
			characterAPI.PATCH("/", routePatchCharacter)
			characterAPI.DELETE("/", routeDeleteCharacter)
			characterAPI.GET("/gallery", routeGetCharacterGallery)
			characterAPI.POST("/gallery", routePostCharacterGallery)
			characterAPI.DELETE("/gallery", routeDeleteFromCharacterGallery)
			characterAPI.GET("/gallery/:originid", routeGetCharacterGalleryByOrigin)
		}
		// Try to refrain from allowing direct creation of characters or direct addition to the gallery,
		// as both are (or should be) managed by other services.

		api.POST("/media/", routePostMedia)
		// Should we change the URL on this to /api/characters/ for consistency?
		api.POST("/character/", routePostCharacter)
	}
}
