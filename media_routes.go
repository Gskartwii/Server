package main

import (
	"strconv"

	"github.com/gin-gonic/gin"
)

// routeGetMedia: impl for GET /api/media/:mediaid
func routeGetMedia(c *gin.Context) {
	mediaid, err := strconv.Atoi(c.Param("mediaid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad media id format",
		})
		return
	}
	var result media
	err = getMedia.Get(&result, mediaid)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Database error",
		})
	} else if result.ID == 0 {
		c.JSON(404, gin.H{
			"error": "No such media id",
		})
	} else {
		c.JSON(200, result)
	}
}

// routeDeleteMedia: impl for DELETE /api/media/:mediaid
func routeDeleteMedia(c *gin.Context) {
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}
	mediaid, err := strconv.Atoi(c.Param("mediaid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad media id format",
		})
		return
	}
	originid, err := redisConnection.Do("GET", "trusted-media:"+auth)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Internal database error",
		})
		return
	}
	originint, err := strconv.Atoi(originid.(string))
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Internal database error",
		})
		return
	}
	deleteMedia.Exec(mediaid, originint)
	c.JSON(200, gin.H{})
	// Maybe I shouldn't be letting the database do the verification
	// But I'm going to anyway.
}

// routePostMedia: impl for POST /media
func routePostMedia(c *gin.Context) {
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}
	originid, err := redisConnection.Do("EXISTS", "trusted-media:"+auth)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Internal database error",
		})
		return
	}
	var intoriginid = originid.(uint64)
	if intoriginid == 0 {
		c.JSON(403, gin.H{
			"error": "Your account may not add media manually",
		})
		return
	}
	// Identify the media type being uploaded
	// Get the media with c.FormFile("media")
	// Process the media and place it somewhere
	// Accept icons from c.FormFile("icon")
	// Insert all of the (non-nullable) info into the database
}
