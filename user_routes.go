package main

import (
	"crypto/sha512"
	"crypto/subtle"
	"encoding/binary"
	"strconv"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/pbkdf2"
)

// routeLogin: Impl for POST /login
// Actual login -> Token
// Consider /auth for non-users
func routeLogin(c *gin.Context) {
	user := c.PostForm("User")
	pass := c.PostForm("Password")
	result := new(userAccount)
	err := findLogin.QueryRowx(user).StructScan(result)
	if err != nil {
		c.JSON(401, gin.H{
			"error": "Invalid username or password",
		})
		return
	}
	userID := make([]byte, 4)
	binary.LittleEndian.PutUint64(userID, result.ID)
	phash := pbkdf2.Key([]byte(pass), userID, 4096, 32, sha512.New)
	if subtle.ConstantTimeCompare(result.PassHash, phash) == 0 {
		c.JSON(401, gin.H{
			"error": "Invalid username or password",
		})
		return
	}
	sessionidstr := generateBearerSessionDB(redisConnection)
	redisConnection.Do("SET", "sessions:"+sessionidstr, result.ID)
	redisConnection.Do("SADD", "active_sessions:"+(string)(result.ID), sessionidstr)
	c.JSON(200, gin.H{
		"token": sessionidstr,
	})
}

// routeRegister: Impl for POST /register
// Register action -> Confirm, token
// Silent Captcha https://developers.google.com/recaptcha/docs/versions
func routeRegister(c *gin.Context) {
	user := c.PostForm("User")
	pass := c.PostForm("Password")
	var result userAccount
	var count int
	err := existsLogin.QueryRow(user).Scan(&count)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Internal database error",
		})
		return
	}
	if count == 1 {
		c.JSON(409, gin.H{
			"error": "Username is already taken",
		})
		return
	}
	userSnowflake := snowgen.Generate().Int64()
	salt := make([]byte, 4)
	binary.LittleEndian.PutUint64(salt, (uint64)(userSnowflake))
	phash := pbkdf2.Key([]byte(pass), salt, 4096, 32, sha512.New)
	result = userAccount{
		Username: user,
		PassHash: phash,
		ID:       (uint64)(userSnowflake),
	}
	tx, err := sqlConnection.Beginx()
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Failed to begin database transaction",
		})
		return
	}
	_, err = tx.Stmtx(createLogin).Exec((uint64)(userSnowflake), user, phash)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Internal database error",
		})
		tx.Rollback()
		return
	}
	_, err = tx.Stmtx(createUserData).Exec((uint64)(userSnowflake), user)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Internal database error",
		})
		tx.Rollback()
		return
	}
	err = tx.Commit()
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Failed to commit database transaction",
		})
		return
	}
	sessionidstr := generateBearerSessionDB(redisConnection)
	redisConnection.Do("SET", "sessions:"+sessionidstr, userSnowflake)
	redisConnection.Do("SADD", "active_sessions:"+(string)(result.ID), sessionidstr)
	c.SetCookie("token", sessionidstr, 2419200, "/", "ibexsuite.com", true, false)
	c.JSON(200, gin.H{
		"session_token": sessionidstr,
	})
}

// routeGetSelfUser: impl for GET /api/user/@me
func routeGetSelfUser(c *gin.Context) {
	// Identify when using a bearer token; user token for external sites
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(404, gin.H{
			"error": "Not logged in",
		})
		return
	}
	result, _ := redisConnection.Do("GET", "sessions:"+auth)
	c.JSON(200, gin.H{
		"id": result.(string),
	})
}

// routeGetUser: impl for GET /api/user/:userid
func routeGetUser(c *gin.Context) {
	/* auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"success": false,
			"error":   "Not logged in",
		})
		return
	} */
	userid, err := strconv.Atoi(c.Param("userid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad userid format",
		})
		return
	}
	result := new(userData)
	err = getUserData.QueryRowx(userid).StructScan(&result)
	// Check id to assert that shit exists and was unmarshalled properly
	if err != nil || result.ID == 0 {
		c.JSON(404, gin.H{
			"error": "No user matching the userid",
		})
		return
	}
	// Filter out everything that can't be seen by setting things to defaults
	c.JSON(200, result)
}

// routePatchUser: impl for PATCH /api/user/:userid
func routePatchUser(c *gin.Context) {
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}

	// TODO
}

// routeGetFursona: impl for GET /api/user/:userid/fursona
func routeGetFursona(c *gin.Context) {
	userid, err := strconv.Atoi(c.Param("userid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad userid format",
		})
		return
	}
	var result character
	err = getFursona.Get(&result, userid)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Database error",
		})
	} else if result.ID == 0 {
		c.JSON(404, gin.H{
			"error": "User has no fursona",
		})
	} else {
		c.JSON(200, result)
	}
}

// routeGetUserGallery: impl for GET /api/user/:userid/gallery
func routeGetUserGallery(c *gin.Context) {
	// Group by origin site, paginate. /* auth := c.GetHeader("Authorization") if auth == "" { c.JSON(403, gin.H{ "success": false, "error":   "Not logged in", }) return } */
	/*
		[{
			_id int64,
			origin int64,
			icon string,
			sample string,
			original string,
			source string
		}, ...]
	*/
	userid, err := strconv.Atoi(c.Param("userid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad userid format",
		})
		return
	}
	var result []media
	err = getUserMedia.Select(&result, userid)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Database error",
		})
	} else {
		c.JSON(200, result)
	}
}

// routePostGallery: impl for POST /api/user/:userid/gallery
func routePostUserGallery(c *gin.Context) {
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}
	// Posting into the library creates a link for an existing media entry
}

// routeGetGalleryByOrigin: impl for GET /api/user/:userid/gallery/:origin
func routeGetUserGalleryByOrigin(c *gin.Context) {
	userid, err := strconv.Atoi(c.Param("userid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad userid format",
		})
		return
	}
	originid, err := strconv.Atoi(c.Param("origin"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad origin id",
		})
	}
	var result []media
	err = getUserMediaByOrigin.Select(&result, userid, originid)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Database error",
		})
	} else {
		c.JSON(200, result)
	}
}

// routeGetCharacters: impl for GET /api/user/:userid/characters
func routeGetUserCharacters(c *gin.Context) {
	// Currently, assuming that characters are only coming from the creative site.
	// But I'm not sure.
	userid, err := strconv.Atoi(c.Param("userid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad userid format",
		})
		return
	}
	var result []character
	err = getUserCharacters.Select(&result, userid)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Database error",
		})
	} else {
		c.JSON(200, result)
	}
}

// routePostCharacters: impl for POST /api/user/:userid/characters
func routePostUserCharacters(c *gin.Context) {
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}
	userid, err := strconv.Atoi(c.Param("userid"))
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Bad userid format",
		})
		return
	}
	name := c.PostForm("name")
	species := c.PostForm("species")
	if name == "" || species == "" {
		c.JSON(400, gin.H{
			"error": "Name and species can not be empty",
		})
		return
	}
	characterSnowflake := snowgen.Generate().Int64()
	_, err = createCharacterWithOwner.Exec(characterSnowflake, name, species, userid)
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Internal database error",
		})
	} else {
		c.JSON(200, gin.H{
			"new_id": characterSnowflake,
		})
	}
}

// routePatchCharacter: impl for PATCH /api/user/:userid/character/:charid
func routePatchUserCharacter(c *gin.Context) {

}

// routeDeleteCharacter: impl for DELETE /api/user/:userid/character/:charid
func routeDeleteUserCharacter(c *gin.Context) {
	// Disown the character.
	auth := c.GetHeader("Authorization")
	if auth == "" {
		c.JSON(403, gin.H{
			"error": "Not logged in",
		})
		return
	}

}
